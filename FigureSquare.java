package Task3;

public class FigureSquare extends Figure {

    @Override
    public int calcSquare(int side) {
        int squareDouble = (int) (side*4);
        System.out.println("Square of square is:" + squareDouble);
        return squareDouble;
    }

    @Override
    public int calcPerimeter(int side) {
        int squareDouble = (int) (side*side);
        System.out.println("Perimeter of square is:" + squareDouble);
        return squareDouble;
    }

}