package Task3;

public abstract class Figure {

    public abstract int calcSquare(int value);

    public abstract int calcPerimeter(int value);

}