package Task3;

public class FigureCircle extends Figure{

    @Override
    public int calcSquare(int radius) {
        int squareDouble = (int)(radius*radius*Math.PI);
        System.out.println("Square of circle is:" + squareDouble);
        return squareDouble;
    }

    @Override
    public int calcPerimeter(int radius) {
        int squareDouble = (int)(2*Math.PI*radius);
        System.out.println("Perimeter of circle is:" + squareDouble);
        return squareDouble;
    }

}