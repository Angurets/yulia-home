package Task3;

public class Test {

    public static void main(String[] args) {

        FigureCircle circle = new FigureCircle();
        circle.calcSquare(7);
        circle.calcPerimeter(7);

        FigureSquare square = new FigureSquare();
        square.calcSquare(9);
        square.calcPerimeter(9);

    }

}